package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.internal.view.SupportSubMenu;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    private String salle;
    private String poste;
    private String DISTANCIEL;
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String tmp[] = getResources().getStringArray(R.array.list_salles);
        DISTANCIEL = tmp[0];
        poste = "";
        salle = DISTANCIEL;
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        Spinner spSalle = getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = getActivity().findViewById(R.id.spPoste);

        ArrayAdapter<CharSequence> spSalleadapter = ArrayAdapter.createFromResource(getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        spSalleadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(spSalleadapter);

        ArrayAdapter<CharSequence> spPosteadapter = ArrayAdapter.createFromResource(getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        spPosteadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter(spPosteadapter);

        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            EditText tvLogin = getActivity().findViewById(R.id.tvLogin);
            if(tvLogin.getText().toString().equals("")){
                Toast t = Toast.makeText(getActivity(),
                        "Erreur Login", Toast.LENGTH_LONG);
                t.show();
            } else {
                Toast t = Toast.makeText(getActivity(),
                        tvLogin.getText(), Toast.LENGTH_LONG);
                t.show();
                NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
            }
        });

        update();
        // TODO Q9
    }

    public void update(){
        Spinner spSalle = getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = getActivity().findViewById(R.id.spPoste);
        if(spSalle.getSelectedItem().toString().equals("Distanciel")){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation(spSalle.getSelectedItem().toString());

        } else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(spSalle.getSelectedItem().toString() +" : "+ spPoste.getSelectedItem());
        }
    }
    // TODO Q9
}