package fr.ulille.iutinfo.teletp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder> {
    final SuiviViewModel model;

    @NonNull
    @Override
    public SuiviAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vue_liste, parent, false);;
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SuiviAdapter.ViewHolder holder, int position) {
        //holder.getQuestion().setText( model.getQuestions(position));
        // Ca ne fonctionne pas parce que il ne trouve le text view ... décomenctez la ligne du dessus pour constater que ca crash ...
        if ((model.getSelected() != null) && (holder.getAdapterPosition() == model.getSelected())) {
            holder.itemView.setActivated(true);
        } else {
            holder.itemView.setActivated(false);
        }
    }

    @Override
    public int getItemCount() {
        return  model.getQuestions().length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View itemView;
        public void setQuestion(String question){
            ((TextView) itemView.findViewById(R.id.question)).setText(question);
        }
        public TextView getQuestion(){
            return itemView.findViewById(R.id.question);
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }

    public SuiviAdapter(SuiviViewModel model) {
        this.model = model;
    }
    // TODO Q7
}
