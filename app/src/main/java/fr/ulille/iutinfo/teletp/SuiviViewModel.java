package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class SuiviViewModel extends AndroidViewModel {

    private MutableLiveData<String> liveLocalisation;
    private MutableLiveData<String> liveUsername;
    private MutableLiveData<Integer> nextQuestion;
    private String[] questions;

    public SuiviViewModel(Application application) {
        super(application);
        liveLocalisation = new MutableLiveData<>();
        liveUsername = new MutableLiveData<>();
        nextQuestion = new MutableLiveData<>();
    }

    public LiveData<String> getLiveUsername() {
        return liveUsername;
    }

    public void setUsername(String username) {
        liveUsername.setValue(username);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Username : " + username, Toast.LENGTH_LONG);
        toast.show();
    }

    private MutableLiveData<Integer> getLiveSelected() {
        if (nextQuestion == null) {
            nextQuestion = new MutableLiveData<>();
        }
        return nextQuestion;
    }

    public Integer getSelected() {
        return getLiveSelected().getValue();
    }

    public String getUsername() {
        return liveUsername.getValue();
    }
    public LiveData<String> getLiveLocalisation() {
        return liveLocalisation;
    }

    public void setLocalisation(String localisation) {
        liveLocalisation.setValue(localisation);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Localisation : " + localisation, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getLocalisation() {
        return liveLocalisation.getValue();
    }

    void initQuestions(Context context){
        this.questions = context.getResources().getStringArray(R.array.list_questions);
    }

    String getQuestions(int position){
        return this.questions[position];
    }
    String[] getQuestions(){return this.questions;}

    LiveData<Integer> getLiveNextQuestion(){
        return this.nextQuestion;
    }

    void setNextQuestion(Integer nextQuestion){
        this.nextQuestion.setValue(nextQuestion);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "NextQuestion : " + this.nextQuestion, Toast.LENGTH_LONG);
        toast.show();
    }

    Integer getNextQuestion(){
        return this.nextQuestion.getValue();
    }

}
